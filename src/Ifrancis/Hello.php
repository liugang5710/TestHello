<?php

namespace Ifancis\Hello;

class Hello
{
    public function tip() {
        echo "hello world";
    }

    static public function tipInfo() {
        echo "hello world";
    }
}
